'use strict'
var Router = require('koa-router')
// 引入需要的表
var Admin = require('../app/controllers/admin')
var Department = require('../app/controllers/department')
var Staff = require('../app/controllers/staff')
var Checking = require('../app/controllers/checking')
var Salary = require('../app/controllers/salary')
var Task = require('../app/controllers/task')
var Notice = require('../app/controllers/notice')
var WorkCard = require('../app/controllers/workCard')
var Account = require('../app/controllers/account')
var Records = require('../app/controllers/records')
var Company = require('../app/controllers/company')
var Setting = require('../app/controllers/setting')

module.exports = function() {
  // 配置接口 api 前缀
  var router = new Router({
    prefix: '/api'
  })

  // admin 用户登录
  router.post('/login', Admin.login)

  // 部门管理
  router.get('/department/list', Department.inquire)
  router.post('/department/add', Department.add)
  router.patch('/department/update', Department.update)
  router.delete('/department/delete', Department.remove)

  // 员工管理
  router.get('/staff/list', Staff.inquire)
  router.post('/staff/add', Staff.add)
  router.patch('/staff/update', Staff.update)
  router.delete('/staff/delete', Staff.remove)

  // 假勤管理
  router.get('/checking/list', Checking.inquire)
  router.post('/checking/add', Checking.add)
  router.patch('/checking/update', Checking.update)
  router.delete('/checking/delete', Checking.remove)

  // 工资管理
  router.get('/salary/list', Salary.inquire)
  router.post('/salary/add', Salary.add)
  router.patch('/salary/update', Salary.update)
  router.delete('/salary/delete', Salary.remove)

  // 任务管理
  router.get('/task/list', Task.inquire)
  router.post('/task/add', Task.add)
  router.patch('/task/update', Task.update)
  router.delete('/task/delete', Task.remove)

  // 公告通知
  router.get('/notice/list', Notice.inquire)
  router.post('/notice/add', Notice.add)
  router.patch('/notice/update', Notice.update)
  router.delete('/notice/delete', Notice.remove)

  // 工牌管理
  router.get('/workCard/list', WorkCard.inquire)
  router.post('/workCard/add', WorkCard.add)
  router.patch('/workCard/update', WorkCard.update)
  router.delete('/workCard/delete', WorkCard.remove)

  // 企业账户
  router.get('/account/list', Account.inquire)

  // 资金流水记录
  router.get('/records/list', Records.inquire)
  router.post('/records/add', Records.add)
  router.patch('/records/update', Records.update)

  // 企业设置
  router.get('/company/list', Company.inquire)
  router.patch('/company/update', Company.update)

  // 系统设置
  router.get('/setting/list', Setting.inquire)
  router.patch('/setting/update', Setting.update)

  // 管理员管理
  router.get('/admin/list', Admin.inquire)
  router.post('/admin/add', Admin.add)
  router.patch('/admin/update', Admin.update)
  router.delete('/admin/delete', Admin.remove)
  router.post('/admin/upload-avatar/', Admin.updateAvatar)

  // 登出
  router.get('/logout', Admin.logout)

  return router
}
