'use strict'
var mongoose = require('mongoose')
var Task = mongoose.model('task')

exports.inquire = async (ctx, next) => {
  var offset = parseInt(ctx.query.offset) || 0
  var count = parseInt(ctx.query.limit) || 15
  var queryArray = await Task.find()
      .skip(offset)
      .limit(count);
  var total = await Task.estimatedDocumentCount()
  ctx.body = {
    success: true,
    objects: queryArray,
    meta: {
      total_count: total
    }
  }
}
exports.add = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var data = new Task({
      title: body.title,
      describe: body.describe,
      startDate: body.startDate,
      principal: body.principal,
      status: body.status,
      percentage: body.percentage
    });
    data.save();
    ctx.body = {
      success: true,
      message: '添加成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.update = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var msg = await Task.findByIdAndUpdate(body._id, {
      title: body.title,
      describe: body.describe,
      startDate: body.startDate,
      principal: body.principal,
      status: body.status,
      percentage: body.percentage
    })
    ctx.body = {
      success: true,
      message: '编辑成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.remove = async (ctx, next) => {
  try {
    var id = ctx.query && ctx.query.id
    if(id) {
      // var Task = await Task.findById(id);
      var msg = Task.findOneAndRemove({_id: id}).exec();
      if(msg){
        ctx.body = {
          success: true,
          message: '删除成功'
        }
      }else{
        ctx.body = {
          success: false,
          message: '没有该任务！'
        }
      }
    }
  } catch (error) {
    console.log('删除失败');
  }
}