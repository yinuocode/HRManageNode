'use strict'
var mongoose = require('mongoose')
var Notice = mongoose.model('notice')

exports.inquire = async (ctx, next) => {
  var offset = parseInt(ctx.query.offset) || 0
  var count = parseInt(ctx.query.limit) || 15
  var queryArray = await Notice.find()
      .sort({
        'createAt': -1
      })
      .skip(offset)
      .limit(count);
  var total = await Notice.estimatedDocumentCount()
  ctx.body = {
    success: true,
    objects: queryArray,
    meta: {
      total_count: total
    }
  }
}
exports.add = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var data = new Notice({
      title: body.title,
      initiator: body.initiator,
      describe: body.describe
    });
    data.save();
    ctx.body = {
      success: true,
      message: '添加成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.update = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var msg = await Notice.findByIdAndUpdate(body._id, {
      title: body.title,
      describe: body.describe
    })
    ctx.body = {
      success: true,
      message: '编辑成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.remove = async (ctx, next) => {
  try {
    var id = ctx.query && ctx.query.id
    if(id) {
      // var Notice = await Notice.findById(id);
      var msg = Notice.findOneAndRemove({_id: id}).exec();
      if(msg){
        ctx.body = {
          success: true,
          message: '删除成功'
        }
      }else{
        ctx.body = {
          success: false,
          message: '没有该项！'
        }
      }
    }
  } catch (error) {
    console.log('删除失败');
  }
}