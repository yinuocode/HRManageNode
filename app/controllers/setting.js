'use strict'
var mongoose = require('mongoose')
var Setting = mongoose.model('setting')

exports.inquire = async (ctx, next) => {
  var queryArray = await Setting.find();
  ctx.body = queryArray[0];
}
exports.update = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var obj = {
      isCheck: body.isCheck,
      checkDay: body.checkDay,
      isSalary: body.isSalary,
      salaryDay: body.salaryDay,
      isContract: body.isContract,
      isProbation: body.isProbation,
      staffBirthday: body.staffBirthday,
      companyBirthday: body.companyBirthday
    }
    if(body._id) {
      var msg = await Setting.findByIdAndUpdate(body._id, obj);
      ctx.body = {
        success: true,
        message: '修改成功'
      }
    }else {
      var data = new Setting(obj);
      data.save();
      ctx.body = {
        success: true,
        message: '添加成功'
      }
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}