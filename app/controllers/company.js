'use strict'
var mongoose = require('mongoose')
var Company = mongoose.model('company')

exports.inquire = async (ctx, next) => {
  var queryArray = await Company.find();
  ctx.body = queryArray[0];
}
exports.update = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var obj = {
      title: body.title,
      logo: body.logo,
      address: body.address,
      website: body.website,
      startDate: body.startDate,
      contact: body.contact,
      weixin: body.weixin,
      qq: body.qq,
      describe: body.describe
    }
    if(body._id) {
      var msg = await Company.findByIdAndUpdate(body._id, obj);
      ctx.body = {
        success: true,
        message: '修改成功'
      }
    }else {
      var data = new Company(obj);
      data.save();
      ctx.body = {
        success: true,
        message: '修改成功'
      }
    }
    ctx.body = {
      success: true,
      message: '修改成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}