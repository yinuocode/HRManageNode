'use strict'
var mongoose = require('mongoose')
var Account = mongoose.model('account')

exports.inquire = async (ctx, next) => {
  var queryArray = await Account.find();
  ctx.body = queryArray
}