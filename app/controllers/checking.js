'use strict'
var mongoose = require('mongoose')
var Checking = mongoose.model('checking')

exports.inquire = async (ctx, next) => {
  var offset = parseInt(ctx.query.offset) || 0
  var count = parseInt(ctx.query.limit) || 15
  var queryArray = await Checking.find()
      .sort({
        'createAt': -1
      })
      .skip(offset)
      .limit(count);
  var total = await Checking.estimatedDocumentCount()
  ctx.body = {
    success: true,
    objects: queryArray,
    meta: {
      total_count: total
    }
  }
}
exports.add = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var data = new Checking({
      rule: body.rule,
      name: body.name,
      holiday: body.holiday,
      duration: body.duration,
      documents: body.documents,
      dockPay: body.dockPay,
      payPproportion: body.payPproportion,
      note: body.note
    });
    data.save();
    ctx.body = {
      success: true,
      message: '添加成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.update = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var msg = await Checking.findByIdAndUpdate(body._id, {
      rule: body.rule,
      name: body.name,
      holiday: body.holiday,
      duration: body.duration,
      documents: body.documents,
      dockPay: body.dockPay,
      payPproportion: body.payPproportion,
      note: body.note
    })
    ctx.body = {
      success: true,
      message: '编辑成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.remove = async (ctx, next) => {
  try {
    var id = ctx.query && ctx.query.id
    if(id) {
      // var Checking = await Checking.findById(id);
      var msg = Checking.findOneAndRemove({_id: id}).exec();
      if(msg){
        ctx.body = {
          success: true,
          message: '删除成功'
        }
      }else{
        ctx.body = {
          success: false,
          message: '没有该项！'
        }
      }
    }
  } catch (error) {
    console.log('删除失败');
  }
}