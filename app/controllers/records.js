'use strict'
var mongoose = require('mongoose')
var Records = mongoose.model('records')

exports.inquire = async (ctx, next) => {
  var offset = parseInt(ctx.query.offset) || 0
  var count = parseInt(ctx.query.limit) || 15
  var type = ctx.query.type?{type: ctx.query.type}:{};
  var queryArray = await Records.find(type)
      .skip(offset)
      .limit(count);
  var total = await Records.estimatedDocumentCount()
  ctx.body = {
    success: true,
    objects: queryArray,
    meta: {
      total_count: total
    }
  }
}
exports.add = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var data = new Records({
      title: body.title,
      type: body.type,
      money: body.money,
      status: body.status,
      opposite: body.opposite,
      remark: body.remark
    });
    data.save();
    ctx.body = {
      success: true,
      message: '添加成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.update = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var msg = await Records.findByIdAndUpdate(body._id, {
      title: body.title,
      type: body.type,
      money: body.money,
      status: body.status,
      opposite: body.opposite,
      remark: body.remark
    })
    ctx.body = {
      success: true,
      message: '修改成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}