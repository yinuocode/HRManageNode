'use strict'
var mongoose = require('mongoose')
var Salary = mongoose.model('salary')
exports.inquire = async (ctx, next) => {
  var offset = parseInt(ctx.query.offset) || 0
  var count = parseInt(ctx.query.limit) || 15
  var queryArray = await Salary.find()
      .skip(offset)
      .limit(count);
  var total = await Salary.estimatedDocumentCount()
  ctx.body = {
    success: true,
    objects: queryArray,
    meta: {
      total_count: total
    }
  }
}
exports.add = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var data = new Salary({
      name: body.name,
      department: body.department,
      salary: body.salary,
      allowance: body.allowance
    });
    data.save();
    ctx.body = {
      success: true,
      message: '添加成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.update = async (ctx, next) => {
  try {
    var body = ctx.request.body;
    var msg = await Salary.findByIdAndUpdate(body._id, {
      name: body.name,
      department: body.department,
      salary: body.salary,
      allowance: body.allowance
    })
    ctx.body = {
      success: true,
      message: '编辑成功'
    }
  } catch (error) {
    ctx.body = {
      success: false,
      message: '服务器错误'
    }
  }
}
exports.remove = async (ctx, next) => {
  try {
    var id = ctx.query && ctx.query.id
    if(id) {
      // var Salary = await Salary.findById(id);
      var msg = Salary.findOneAndRemove({_id: id}).exec();
      if(msg){
        ctx.body = {
          success: true,
          message: '删除成功'
        }
      }else{
        ctx.body = {
          success: false,
          message: '没有该项！'
        }
      }
    }
  } catch (error) {
    console.log('删除失败');
  }
}