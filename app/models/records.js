'use strict'

var mongoose = require('mongoose')
/*
  资金流水表
  title 资金流水标题
  type 资金流水类型（线上或线下）
  money 资金流水金额
  status 资金流水状态
  opposite 流水去向
  remark 流水备注
  创建时间
  修改时间
*/
var RecordsSchema = new mongoose.Schema({
  title: String,
  type: String,
  money: String,
  status: String,
  opposite: String,
  remark: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
})

RecordsSchema.pre('save', function(next) {
  if (this.isNew) {
    this.createAt = this.updateAt = Date.now()
  }else {
    this.updateAt = Date.now()
  }
  next()
})

module.exports = mongoose.model('records', RecordsSchema)