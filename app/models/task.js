'use strict'

var mongoose = require('mongoose')
/*
  任务管理表
  title 任务名
  describe 任务简介
  startDate 开始时间
  principal 任务负责人
  percentage 任务进度百分比
  创建时间
  修改时间
*/
var TaskSchema = new mongoose.Schema({
  title: String,
  describe: String,
  startDate: String,
  percentage: String,
  status: String,
  principal: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
})

TaskSchema.pre('save', function(next) {
  if (this.isNew) {
    this.createAt = this.updateAt = Date.now()
  }else {
    this.updateAt = Date.now()
  }
  next()
})
module.exports = mongoose.model('task', TaskSchema)