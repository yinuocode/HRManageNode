'use strict'

var mongoose = require('mongoose')

var AdminSchema = new mongoose.Schema({
  // 管理员表格
  account: String,
  password: String,
  status: String,
  accessToken: String,
  macAddress: String,
  userName: String,
  avatar: String,
  role: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
});
AdminSchema.pre('save', function(next) {
  if (this.isNew) {
    this.createAt = this.updateAt = Date.now()
  }
  else {
    this.updateAt = Date.now()
  }

  next()
})

module.exports = mongoose.model('admin', AdminSchema)