'use strict'

var mongoose = require('mongoose')
/*
  系统设置表
  isCheck 考勤是否提醒
  checkDay 每月考勤提醒日期设置
  isSalary 工资发放是否提醒
  salaryDay 每月工资发放日提醒设置
  isContract 员工合同到期提醒
  isProbation 试用期到期提醒
  staffBirthday 员工生日提醒
  companyBirthday 公司成立周年提醒
*/
var SettingSchema = new mongoose.Schema({
  isCheck: Boolean,
  checkDay: String,
  isSalary: Boolean,
  salaryDay: String,
  isContract: Boolean,
  isProbation: Boolean,
  staffBirthday: Boolean,
  companyBirthday: Boolean
});

module.exports = mongoose.model('setting', SettingSchema)