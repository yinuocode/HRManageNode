'use strict'

var mongoose = require('mongoose')
/*
  公告通知表
  title 公告标题
  describe 公告内容
  initiator 公告发起人
  创建时间
  修改时间
*/
var NoticeSchema = new mongoose.Schema({
  title: String,
  initiator: String,
  describe: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
})

NoticeSchema.pre('save', function(next) {
  if (this.isNew) {
    this.createAt = this.updateAt = Date.now()
  }else {
    this.updateAt = Date.now()
  }
  next()
})

module.exports = mongoose.model('notice', NoticeSchema)