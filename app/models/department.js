'use strict'

var mongoose = require('mongoose')
/*
  部门表
  部门名称，部门编号，部门状态，部门人数，部门领导，上级部门，部门描述，创建时间，修改时间
*/
var DepartmentSchema = new mongoose.Schema({
  depName: String,
  depCode: String,
  depStatus: String,
  depPersonnel: Number,
  depLead: String,
  depSup: String,
  depDescribe: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
})

DepartmentSchema.pre('save', function(next) {
  if (this.isNew) {
    this.createAt = this.updateAt = Date.now()
  }
  else {
    this.updateAt = Date.now()
  }
  next()
})

module.exports = mongoose.model('department', DepartmentSchema)
