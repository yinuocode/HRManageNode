'use strict'

var mongoose = require('mongoose')
/*
  企业账户表
  balance 余额
  booked 入账
  spending 支出
*/
var AccountSchema = new mongoose.Schema({
  balance: String,
  booked: String,
  spending: String
});

module.exports = mongoose.model('account', AccountSchema)