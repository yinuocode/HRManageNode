'use strict'

var mongoose = require('mongoose')
/*
  考勤管理表
  rule 考勤规则（排班制、坐班制、自由打卡、弹性工作）
  name 考勤人员
  holiday 假期类型（事假、病假、婚假、工伤假、产假、年假、旷工、迟到、其他）
  duration 时长
  documents 单据抄送
  status 状态
  dockPay 是否扣薪
  payPproportion 扣薪比例
  note 备注
  创建时间
  修改时间
*/
var CheckingSchema = new mongoose.Schema({
  rule: String,
  name: String,
  holiday: String,
  duration: String,
  documents: String,
  dockPay: String,
  payPproportion: String,
  note: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
})
CheckingSchema.pre('save', function(next) {
  if (this.isNew) {
    this.createAt = this.updateAt = Date.now()
  }else {
    this.updateAt = Date.now()
  }
  next()
})
module.exports = mongoose.model('checking', CheckingSchema)