'use strict'

var mongoose = require('mongoose')
/*
  企业设置表
  title 公司名
  logo 公司logo
  address 地址
  website 官网
  startDate 成立时间
  contact 联系方式
  weixin 企业微信
  qq 技术支持qq
  describe 公司简介
*/
var CompanySchema = new mongoose.Schema({
  title: String,
  logo: String,
  address: String,
  website: String,
  startDate: String,
  contact: String,
  weixin: String,
  qq: String,
  describe: String
})

module.exports = mongoose.model('company', CompanySchema)