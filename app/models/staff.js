'use strict'

var mongoose = require('mongoose')
/*
  员工表
  职工号、籍贯、部门、姓名、性别、职位、状态、头像、年龄、是否结婚、学历、基本工资、岗位津贴、住址、电话
  创建时间
  修改时间
*/
var StaffSchema = new mongoose.Schema({
  stSerial: String,
  nativePlace: String,
  department: String,
  name: String,
  gender: String,
  post: String,
  status: String,
  avatar: String,
  birthday: String,
  marriage: String,
  education: String,
  salary: String,
  phoneNumber: String,
  address: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
})

StaffSchema.pre('save', function(next) {
  if (this.isNew) {
    this.createAt = this.updateAt = Date.now()
  }else {
    this.updateAt = Date.now()
  }
  next()
})

module.exports = mongoose.model('staff', StaffSchema)