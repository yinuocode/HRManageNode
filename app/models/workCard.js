'use strict'

var mongoose = require('mongoose')
/*
  工牌制作表
  title 工牌名称
  projector 设计师
  style 工牌样式
  status 工牌状态
  remark 工牌备注
*/
var WorkCardSchema = new mongoose.Schema({
  title: String,
  projector: String,
  style: String,
  status: String,
  remark: String
})

module.exports = mongoose.model('workCard', WorkCardSchema)