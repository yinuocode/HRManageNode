'use strict'

var mongoose = require('mongoose')
/*
  薪资管理表
  name 姓名
  workNumber 工号
  department 部门
  salary 当前基本工资
  allowance 当前岗位津贴
  修改时间
*/
var SalarySchema = new mongoose.Schema({
  name: String,
  workNumber: String,
  department: String,
  salary: String,
  allowance: String,
  createAt: {
    type: Date,
    dafault: Date.now()
  },
  updateAt: {
    type: Date,
    dafault: Date.now()
  }
})

SalarySchema.pre('save', function(next) {
if (this.isNew) {
  this.createAt = this.updateAt = Date.now()
}else {
  this.updateAt = Date.now()
}
next()
})

module.exports = mongoose.model('salary', SalarySchema)